public class Casting {

  public static void main(String[] args) {
    // En un anno ubicar 30 perritos
    // Cuantos perritos ubique al mes

    double monthlyDogs = 30.0 / 12.0;
    System.out.println(monthlyDogs);

    // Estimation
    int stimatedMonthlyDogs = (int) monthlyDogs;
    System.out.println(stimatedMonthlyDogs);

    // Exactitud
    int a = 30;
    int b = 12;

    System.out.println((double) a/b);

    // double c = a/b;
    // System.out.println(c);
    
    double c = (double) a/b;
    System.out.println(c);


    // Char 
    char n = '1';
    int nI = n;

    System.out.println(nI);

    short nS = (short) n;
    System.out.println(nS);
  }
  
}
