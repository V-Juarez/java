public class UpdateVariables {

  public static void main(String[] args) {
    int salary = 1000;

    // bono $200
    salary += 200;
    System.out.println(salary);

    // Pension: $50 descuento
    salary -= 50;
    System.out.println(salary);

    // 2 horas extra $30 c/u
    // comida: $45

    salary +=  (30*2) - 45;
    System.out.println(salary);

    // Actualizar cadenas de Texto
    String employeeName = "Anahi Salgado";
    employeeName = employeeName + " Diaz de la Vega";
    System.out.println(employeeName);

    employeeName = "Irene " + employeeName;
    System.out.println(employeeName);

    employeeName = "Hola, Como estas?";
    System.out.println(employeeName);

  }
}
