public class DataTypes {
  
  public static void main(String[] args) {

    int n = 1232343545;

    long l = 12345678901011L;

    double nD =  12343.234234;
    float nF = 12334.233432F;

    var salary = 10000;  // int
    // pension 3%
    var pension = salary * 0.03;  // double
    var totalSalary = salary - pension;

    System.out.println(salary);
    System.out.println(pension);
    System.out.println(totalSalary);

    var employeeName = "Anahi Salgado";
    System.out.println("EMPLOYEE: " + employeeName + " SALARY: " + totalSalary);
  }
}
