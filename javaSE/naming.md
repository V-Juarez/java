LowerCamelCase: para métodos y variables

```java
String isAvailable = true;
```

UpperCamelCase: para Clases, interfaces.
```java
public class Connection{ 
}
```

Snake Case: para constantes

```java
public class Config{
	public static final int MAX_SIZE = 30;
}
```

UpperCamelCase : `Clases`
lowerCamelCase : `variables y métodos`


## Funciones math

```java
public class MathematicOperations {
    public static void main(String[] args) {
        //Valores enteros
        int a = 5;
        int b = 10;

        //Valores con decimales
        double i = 2.1;
        double j = 3.5;

        //Redondea hacia arriba
        System.out.println(Math.ceil(i));

        //Redondea hacia abajo
        System.out.println(Math.floor(j));

        //Devuelve el valor mayor
        System.out.println(Math.max(a,b));

        //Imprime un numero elevado a otro
        System.out.println(Math.pow(a,b));

        //Devuelve el valor absoluto de un argumento dado
        System.out.println(Math.abs(j));

        //Devuelve la suma de sus argumentos
        System.out.println(Math.addExact(a,b));

        //Devuelve la tangente del arco de un angulo (-pi/2 y pi/2)
        System.out.println(Math.atan(j));

        //Devuelve la raiz cubica de un valor double
        System.out.println(Math.cbrt(i));

        //Devuvleve el coseno hiperbolico de un valor double
        System.out.println(Math.cosh(j));

        //Devuelve el coseno trigonometrico de un angulo
        System.out.println(Math.cos(j));

        //Devuelve el numero elevado a la potencia de un valor double
        System.out.println(Math.exp(i));
    }
}
```

## Cast entre variables
.::Conversión Automatica::.
```java
byte -> short
short -> int
char -> int
int -> long
long -> float
float -> double
```

.::Cast Necesario::.
```java
double -> float
float -> long
long -> int
int -> short
short -> byte

char <-> byte
char <-> short
int -> char
```

## comment Java, it's very important

![](./img/comment-java.webp)

## Bucle for

![](img/while.webp)

Java:
```java
for(int i = 5; i >= 0; i--){
	for(int j = 0; j <= i; j++){
		System.out.print("*");
	}
	System.out.print("\n");
}
```

- `break;` para el loop
- `continue;` para la iteración
- `return val;` regresa un dato (generalmente de una función)

Ejemplos

`Break`

```java
for (byte i = 0; i<=10; i++) {
	if (i == 4) {
		break;
	}
	System.out.println("i: " + i);
}
```
Este for-i loop va a hacer que cuando i sea igual a 4, el loop se va romper.
```sh
output:

0
1
2
3
Continue
```
```java
for (byte i = 0; i<=10; i++) {
	if (i == 4) {
		continue;
	}
  System.out.println("i = " + i);
}
```

Este `for-i` loop va a imprimir los numeros del 0-9, pero cuando llegue al `4 (i = 4)`, va a partir la iteración, por ende todo lo que esté después del continue; y dentro del scope del `for-i` loop, no va a ser ejecutado.

```sh
output

0
1
2
3
5
6
7
8
9
```
Como se pueden dar cuenta, falta el `4`, y esto es gracias al `continue`.

## Array
![](./img/array.gif)

> ✨ Los arrays son objetos que permiten almacenar más de una variable.
