/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.platzi.mensaje_app;

import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author mr
 */
public class Inicio {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int opcion = 0;
        do{
            System.out.println("-------------");
            System.out.println("Aplicacion de mensaje");
            System.out.println("1. Crear mensaje");
            System.out.println("2. listar mensajes");
            System.out.println("3. editar mensaje");
            System.out.println("4. editar mensaje");
            System.out.println("5. eliminar mensaje");
            System.out.println("5. salir");
            System.out.println("--------------");
            opcion = sc.nextInt();

            switch (opcion) {
                case 1:
                    MensajeService.crearMensaje();
                    break;
                case 2:
                    MensajeService.listarMensajes();
                    break;
                case 3:
                    MensajeService.borrarMensaje();
                    break;
                case 4:
                    MensajeService.editarMensaje();
                    break;
                default:
                    break;
            }
        } while(opcion != 5);



//        System.out.println("Hello World!");
//        Conexion conexion = new Conexion();
//
//        try(Connection cnx = conexion.get_connection()){
//
//        } catch (Exception e) {
//            System.out.println(e);
//        }
    }
}
